package searchcore

import (
	"github.com/blugelabs/bluge/analysis"
	"github.com/blugelabs/bluge/analysis/lang/de"
	"github.com/blugelabs/bluge/analysis/lang/en"
	"github.com/blugelabs/bluge/analysis/lang/fr"
	"github.com/blugelabs/bluge/analysis/lang/it"
	"github.com/blugelabs/bluge/analysis/tokenizer"
)

var (
	analyzers          map[string]*analysis.Analyzer
	availableLanguages []string
)

func init() {
	analyzers = map[string]*analysis.Analyzer{
		"en": withURLTokenizer(en.NewAnalyzer()),

		"it": withURLTokenizer(it.Analyzer()),
		"fr": withURLTokenizer(fr.Analyzer()),
		"de": withURLTokenizer(de.Analyzer()),
	}
	for lang := range analyzers {
		availableLanguages = append(availableLanguages, lang)
	}
}

func withURLTokenizer(a *analysis.Analyzer) *analysis.Analyzer {
	a.Tokenizer = tokenizer.NewWebTokenizer()
	return a
}

func getAnalyzer(lang string) *analysis.Analyzer {
	a, ok := analyzers[lang]
	if !ok {
		a = analyzers["en"]
	}
	return a
}
