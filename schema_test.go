package searchcore

import (
	"testing"
)

func TestFlatten(t *testing.T) {
	testSchema := `{
  "languages": ["it", "en"],
  "fields": {
    "title": {
      "type": "text"
    },
    "body": {
      "type": "text"
    }
  }
}`
	testMap := map[string]interface{}{
		"id": 42,
		"body": map[string]interface{}{
			"en": "this is a document",
			"it": "questo è un documento",
		},
		"title": map[string]interface{}{
			"en": "title",
			"it": "titolo",
		},
		"tags": []interface{}{
			"tag1",
			"tag2",
			"tag3",
		},
		"nested": map[string]interface{}{
			"inner": map[string]interface{}{
				"list": []interface{}{
					"one",
					"two",
				},
			},
		},
	}

	schema, err := DecodeSchema([]byte(testSchema))
	if err != nil {
		t.Fatalf("Schema json unmarshal: %v", err)
	}

	hasBody := false
	err = flatten(schema, testMap, func(key string, value interface{}, field *Field) error {
		//t.Logf("%s = %v (%v, %s)", key, value, field, lang)
		if key == "body" {
			hasBody = true
			if field == nil {
				t.Error("nil Field on body")
			}
		}
		return nil
	})
	if err != nil {
		t.Errorf("flatten() error: %v", err)
	}
	if !hasBody {
		t.Error("the flatten() function was not called with body.* fields")
	}
}
