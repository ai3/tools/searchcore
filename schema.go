package searchcore

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/blugelabs/bluge"
	"github.com/blugelabs/bluge/analysis"
)

// A JSON-parsed map.
type JSONDoc map[string]interface{}

// Field types.
const (
	KeywordField  = "keyword"
	TextField     = "text"
	NumericField  = "numeric"
	DateTimeField = "datetime"
)

// Field is a field in the schema.
type Field struct {
	Type      string  `json:"type"`
	Boost     float64 `json:"boost,omitempty"`
	NoIndex   bool    `json:"no_index,omitempty"`
	NoStore   bool    `json:"no_store,omitempty"`
	Aggregate bool    `json:"aggregate,omitempty"`
}

func (f *Field) GetBoost() float64 {
	if f.Boost == 0 {
		return 1
	}
	return f.Boost
}

func (f *Field) addFlags(field *bluge.TermField) *bluge.TermField {
	if !f.NoStore {
		field = field.StoreValue()
	}
	if f.Aggregate {
		field = field.Aggregatable()
	}
	return field
}

// Analyzer for this field, if any.
func (f *Field) Analyzer(lang string) *analysis.Analyzer {
	if f.Type == TextField {
		return getAnalyzer(lang)
	}
	return nil
}

// Schema defines known field types and other global indexing and
// querying parameters.
type Schema struct {
	IDField         string            `json:"id_field"`
	Languages       []string          `json:"languages"`
	DefaultLanguage string            `json:"default_language"`
	Fields          map[string]*Field `json:"fields"`

	defaultField *Field
}

// DecodeSchema parses a JSON-encoded Schema.
func DecodeSchema(data []byte) (*Schema, error) {
	schema := Schema{
		IDField:         "id",
		Languages:       availableLanguages,
		DefaultLanguage: "en",

		defaultField: &Field{
			Type: KeywordField,
		},
	}
	if err := json.Unmarshal(data, &schema); err != nil {
		return nil, err
	}
	return &schema, nil
}

// GetField returns either the specific field, or the default one.
func (s *Schema) GetField(name string) *Field {
	f, ok := s.Fields[name]
	if !ok {
		f = s.defaultField
	}
	return f
}

func (s *Schema) Document(jdoc JSONDoc) (*bluge.Document, error) {
	docid, ok := jdoc[s.IDField].(string)
	if !ok {
		return nil, fmt.Errorf("missing primary key field '%s'", s.IDField)
	}

	doc := bluge.NewDocument(docid)

	err := flatten(s, jdoc, func(name string, value interface{}, field *Field) error {
		if name == s.IDField {
			return nil
		}
		if field == nil {
			field = s.defaultField
		}
		return s.addFieldToDoc(doc, field, name, value)
	})

	return doc, err
}

func (s *Schema) addFieldToDoc(doc *bluge.Document, field *Field, name string, value interface{}) error {
	if field.NoIndex {
		s, ok := strValue(value)
		if !ok {
			return fmt.Errorf("value for field '%s' is not a string", name)
		}
		doc.AddField(bluge.NewStoredOnlyField(name, []byte(s)))
		return nil
	}

	switch field.Type {
	case KeywordField:
		s, ok := strValue(value)
		if !ok {
			return fmt.Errorf("value for field '%s' is not a string", name)
		}
		doc.AddField(field.addFlags(bluge.NewKeywordField(name, s)))

	case NumericField:
		i, ok := value.(float64)
		if !ok {
			return fmt.Errorf("value for field '%s' is not a number", name)
		}
		doc.AddField(field.addFlags(bluge.NewNumericField(name, i)))

	case TextField:
		m, ok := value.(map[string]interface{})
		if !ok {
			m = map[string]interface{}{s.DefaultLanguage: value}
		}

		for lang, lValue := range m {
			s, ok := strValue(lValue)
			if !ok {
				return fmt.Errorf("value for field '%s' (language '%s') is not a string", name, lang)
			}

			doc.AddField(field.addFlags(
				bluge.NewTextField(langFieldName(name, lang), s).
					WithAnalyzer(field.Analyzer(lang)).
					SearchTermPositions().
					HighlightMatches()))
		}

	default:
		return fmt.Errorf("unknown field type '%s'", field.Type)
	}

	return nil
}

// Coerce a value to string (incl. numbers).
func strValue(value interface{}) (string, bool) {
	switch v := value.(type) {
	case string:
		return v, true
	case float64:
		return strconv.FormatFloat(v, 'g', -1, 64), true
	default:
		return "", false
	}
}

const fieldSeparator = "."

func joinWithSeparator(a, b string) string {
	if a == "" {
		return b
	}
	return a + fieldSeparator + b
}

func langFieldName(name, lang string) string {
	return joinWithSeparator(name, lang)
}

// Map a document to a list of fields, by flattening the key space
// recursively, and call the passed function on every result along
// with the associated Field (which may be nil for fields that are not
// explicitly declared in the Schema).
func flatten(schema *Schema, doc JSONDoc, f func(string, interface{}, *Field) error) error {
	return flattenInternalRec(schema, "", map[string]interface{}(doc), nil, f)
}

func flattenInternalRec(schema *Schema, key string, value interface{}, field *Field, f func(string, interface{}, *Field) error) error {
	if field == nil {
		field = schema.Fields[key]
	}

	switch v := value.(type) {
	case map[string]interface{}:
		// TextField maps are their own standalone value type.
		if field != nil && field.Type == TextField {
			return f(key, value, field)
		}
		for k, vv := range v {
			kk := joinWithSeparator(key, k)
			if err := flattenInternalRec(schema, kk, vv, field, f); err != nil {
				return err
			}
		}

	case []interface{}:
		for _, vv := range v {
			if err := flattenInternalRec(schema, key, vv, field, f); err != nil {
				return err
			}
		}

	default:
		return f(key, value, field)
	}

	return nil
}
