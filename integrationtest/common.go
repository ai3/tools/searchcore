package integrationtest

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"git.autistici.org/ai3/tools/searchcore"
	"github.com/blugelabs/bluge"
)

func NewTestIndex(t *testing.T, schemaJSON string, docsJSON []string) (*searchcore.Index, func()) {
	path, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	schema, err := searchcore.DecodeSchema([]byte(schemaJSON))
	if err != nil {
		t.Fatalf("Schema json unmarshal: %v", err)
	}

	index, err := searchcore.NewIndex(path, schema)
	if err != nil {
		t.Fatalf("NewIndex: %v", err)
	}

	addTestDocs(t, index, docsJSON)

	return index, func() {
		index.Close()
		os.RemoveAll(path)
	}
}

func addTestDocs(t *testing.T, index *searchcore.Index, docsJSON []string) {
	var jsonDocs []searchcore.JSONDoc
	for _, td := range docsJSON {
		var testDoc searchcore.JSONDoc
		json.Unmarshal([]byte(td), &testDoc) // nolint: errcheck
		jsonDocs = append(jsonDocs, testDoc)
	}

	if err := index.AddMany(jsonDocs); err != nil {
		t.Fatalf("AddMany: %v", err)
	}
}

func RunTestSearch(t *testing.T, index *searchcore.Index, q bluge.Query) []string {
	var ids []string
	if err := index.Search(context.Background(), q, func(doc searchcore.JSONDoc) error {
		ids = append(ids, doc["id"].(string))
		return nil
	}); err != nil {
		t.Fatalf("error executing search: %v", err)
	}
	return ids
}
