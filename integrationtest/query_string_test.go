package integrationtest

import (
	"fmt"
	"testing"

	querystr "git.autistici.org/ai3/tools/searchcore/query_string"
	"github.com/google/go-cmp/cmp"
)

type queryStringTestDataType struct {
	schema      string
	docs        []string
	query       string
	queryLang   string
	expectedIDs []string
}

var queryStringTestData = []queryStringTestDataType{
	{
		testSchemaJSON,
		testDocsJSON,
		"body:first",
		"en",
		[]string{"1"},
	},

	{
		testSchemaJSON,
		testDocsJSON,
		"body:\"documento numero due\"",
		"it",
		[]string{"2"},
	},

	{
		testSchemaJSON,
		testDocsJSON,
		"+body:documento -body:due",
		"it",
		[]string{"1"},
	},
}

func testSingleQueryString(t *testing.T, schemaJSON string, docsJSON []string, query, queryLang string, expectedIDs []string) {
	index, cleanup := NewTestIndex(t, schemaJSON, docsJSON)
	defer cleanup()

	q, err := querystr.ParseQueryString(index.Schema, query, querystr.DefaultOptions())
	if err != nil {
		t.Fatalf("Query: %v", err)
	}

	ids := RunTestSearch(t, index, q)
	if diffs := cmp.Diff(ids, expectedIDs); diffs != "" {
		t.Fatalf("bad result: %v", diffs)
	}
}

func TestQueryString_Search(t *testing.T) {
	for _, td := range queryStringTestData {
		td := td
		t.Run(fmt.Sprintf("%s (%s)", td.query, td.queryLang), func(t *testing.T) {
			testSingleQueryString(t, td.schema, td.docs, td.query, td.queryLang, td.expectedIDs)
		})
	}
}
