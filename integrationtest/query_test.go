package integrationtest

import (
	"fmt"
	"testing"

	"git.autistici.org/ai3/tools/searchcore"
	"github.com/google/go-cmp/cmp"
)

var testSchemaJSON = `{
  "id_field": "id",
  "languages": ["it", "en"],
  "default_language": "en",
  "fields": {
    "title": {
      "type": "text",
      "boost": 5
    },
    "body": {
      "type": "text"
    },
    "tag": {
      "type": "keyword"
    }
  }
}`

var testDocsJSON = []string{
	`{
  "id": "1",
  "title": {
    "en": "Document 1",
    "it": "Documento 1"
  },
  "body": {
    "en": "First document. This is document number one.",
    "it": "Primo documento. Questo è il documento numero uno."
  },
  "tag": ["one", "first"]
}`,
	`{
  "id": "2",
  "title": {
    "en": "Document 2",
    "it": "Documento 2"
  },
  "body": {
    "en": "Second document. This is document number two.",
    "it": "Secondo documento. Questo è il documento numero due."
  },
  "tag": ["two", "second"]
}`,
}

type queryTestDataType struct {
	schema      string
	docs        []string
	query       map[string]interface{}
	queryLang   string
	expectedIDs []string
}

var queryTestData = []queryTestDataType{
	// Simple query in one language.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "two",
		},
		"en",
		[]string{"2"},
	},

	// Same document as previous query, but in the other language.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "due",
		},
		"en",
		[]string{"2"},
	},

	// Fetch document 1 using unique term.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "one",
		},
		"en",
		[]string{"1"},
	},

	// Fetch document 2 using unique phrase.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "document number two",
		},
		"en",
		[]string{"2"},
	},

	// Fetch document 1 using unique phrase. The query language
	// shouldn't have an effect because there is no ambiguity that
	// the boost could affect.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "document number two",
		},
		"it",
		[]string{"2"},
	},

	// Fetch document 2 using unique phrase, other language.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"body": "secondo documento",
		},
		"it",
		[]string{"2"},
	},

	// Test that flat documents use the default language.
	{
		testSchemaJSON,
		[]string{
			`{"id": "1", "title": "specialtoken", "body": "irrelevant"}`,
			`{"id": "2", "title": "some title", "body": "also contains the specialtoken"}`,
		},
		map[string]interface{}{
			"title": "specialtoken",
		},
		"en",
		[]string{"1"},
	},

	// Test multi-value keyword field retrieval.
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"tag": "two",
		},
		"en",
		[]string{"2"},
	},
	{
		testSchemaJSON,
		testDocsJSON,
		map[string]interface{}{
			"tag": "second",
		},
		"en",
		[]string{"2"},
	},
}

func testSingleQuery(t *testing.T, schemaJSON string, docsJSON []string, query map[string]interface{}, queryLang string, expectedIDs []string) {
	index, cleanup := NewTestIndex(t, schemaJSON, docsJSON)
	defer cleanup()

	q, err := index.Schema.SimpleQuery(searchcore.JSONDoc(query), queryLang)
	if err != nil {
		t.Fatalf("Query: %v", err)
	}

	ids := RunTestSearch(t, index, q)
	if diffs := cmp.Diff(ids, expectedIDs); diffs != "" {
		t.Fatalf("bad result: %v", diffs)
	}
}

func TestQuery(t *testing.T) {
	for i, td := range queryTestData {
		td := td
		t.Run(fmt.Sprintf("query %d", i+1), func(t *testing.T) {
			testSingleQuery(t, td.schema, td.docs, td.query, td.queryLang, td.expectedIDs)
		})
	}
}
