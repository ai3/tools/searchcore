package searchcore

import (
	"fmt"

	"github.com/blugelabs/bluge"
)

const SameLanguageBoostFactor = 2.0

// SimpleQuery builds a simple AND query out of key/value match (or phrase)
// queries specified as a map.
func (s *Schema) SimpleQuery(qdoc JSONDoc, queryLang string) (bluge.Query, error) {
	query := bluge.NewBooleanQuery()

	err := flatten(s, qdoc, func(name string, value interface{}, field *Field) error {
		if field == nil {
			field = s.defaultField
		}

		svalue, ok := strValue(value)
		if !ok {
			return fmt.Errorf("value for '%s' is not a string", name)
		}

		switch field.Type {
		case KeywordField:
			query.AddMust(
				bluge.NewTermQuery(svalue).
					SetField(name))

		case TextField:
			// Expand the search to all language fields (which is
			// a disjunction query across languages). Query
			// language gets a boost.
			query.AddMust(s.multiLanguageQuery(func(lang string) bluge.Query {
				q := bluge.NewMatchPhraseQuery(svalue).
					SetField(langFieldName(name, lang)).
					SetAnalyzer(field.Analyzer(lang))
				if lang == queryLang {
					q = q.SetBoost(SameLanguageBoostFactor)
				}
				return q
			}))
		}

		return nil
	})

	return query, err
}

// Build a disjunction query for a multilanguage text field, using a generator
// function (called with language as argument).
func (s *Schema) multiLanguageQuery(f func(string) bluge.Query) bluge.Query {
	query := bluge.NewBooleanQuery()
	for _, lang := range s.Languages {
		query.AddShould(f(lang))
	}
	return query
}

// QueryForField builds a bluge.Query by calling a query generator function
// for every actual indexed field, handling multilanguage text
// transparently. The generator function is called with field, field name, and
// field language as arguments. This lets the caller build custom queries that
// adapt to the multi-field structure.
func (s *Schema) QueryForField(name string, qfn func(*Field, string, string) bluge.Query) bluge.Query {
	field := s.GetField(name)
	if field.Type == TextField {
		return s.multiLanguageQuery(func(lang string) bluge.Query {
			return qfn(field, langFieldName(name, lang), lang)
		})
	}
	return qfn(field, name, s.DefaultLanguage)
}
