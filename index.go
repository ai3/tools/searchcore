package searchcore

import (
	"context"
	"fmt"

	"github.com/blugelabs/bluge"
)

// The Index collects documents in a single corpus, and deals with
// storage etc.
type Index struct {
	writer *bluge.Writer

	Schema *Schema
}

func NewIndex(path string, schema *Schema) (*Index, error) {
	config := bluge.DefaultConfig(path)
	writer, err := bluge.OpenWriter(config)
	if err != nil {
		return nil, err
	}
	return &Index{
		writer: writer,
		Schema: schema,
	}, nil
}

func (i *Index) Close() {
	i.writer.Close()
}

func (i *Index) AddMany(jdocs []JSONDoc) error {
	batch := bluge.NewBatch()
	for _, jdoc := range jdocs {
		doc, err := i.Schema.Document(jdoc)
		if err != nil {
			return err
		}
		batch.Insert(doc)
	}
	return i.writer.Batch(batch)
}

func (i *Index) Search(ctx context.Context, query bluge.Query, f func(JSONDoc) error) error {
	reader, err := i.writer.Reader()
	if err != nil {
		return err
	}

	request := bluge.NewAllMatches(query)
	documentMatchIterator, err := reader.Search(ctx, request)
	if err != nil {
		return err
	}
	match, err := documentMatchIterator.Next()
	for err == nil && match != nil {
		doc := make(map[string]interface{})
		err = match.VisitStoredFields(func(field string, value []byte) bool {
			if field == "_id" {
				field = i.Schema.IDField
			}
			doc[field] = string(value)
			return true
		})
		if err != nil {
			return fmt.Errorf("loading stored fields: %w", err)
		}

		if err = f(JSONDoc(doc)); err != nil {
			return err
		}

		match, err = documentMatchIterator.Next()
	}
	return err
}
